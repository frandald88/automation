﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class LoginPage
    {
        public LoginPage()
        {
            PageFactory.InitElements(SprocketPropertiesCollection.driver, this);

        }
    

    [FindsBy(How = How.Id, Using = "ctl00_ContentPlaceHolder1_txtUserName")]
    public IWebElement UserName { get; set; }

    [FindsBy(How = How.Id, Using = "ctl00_ContentPlaceHolder1_txtPassword")]
    public IWebElement PassWord { get; set; }

    [FindsBy(How = How.Id, Using = "ctl00_ContentPlaceHolder1_btnLogin")]
    public IWebElement LoginButton { get; set; }

    [FindsBy(How = How.Id, Using = "btnNewLink")]
    public IWebElement BtnNewWorkOrderLink { get; set; }

        public EAPageObject Login(string userName, string password)
        {
            UserName.EnterText(userName);
            PassWord.EnterText(password);
            LoginButton.Clicks();

            return new EAPageObject();

        }


      }
}
