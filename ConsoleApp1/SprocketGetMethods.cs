﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class SprocketGetMethods
    {
        public static string GetText(IWebElement element /*string element, PropertyType elementtype*/)
        {
            /*if (elementtype == PropertyType.Id)
                return SprocketPropertiesCollection.driver.FindElement(By.Id(element)).GetAttribute("Value");
            if (elementtype == PropertyType.Name)
                return SprocketPropertiesCollection.driver.FindElement(By.Name(element)).GetAttribute("Value");
            if (elementtype == PropertyType.XPath)
                return SprocketPropertiesCollection.driver.FindElement(By.XPath(element)).GetAttribute("Value");
            if (elementtype == PropertyType.CssSelector)
                return SprocketPropertiesCollection.driver.FindElement(By.CssSelector(element)).GetAttribute("Value");
            if (elementtype == PropertyType.LinkText)
                return SprocketPropertiesCollection.driver.FindElement(By.LinkText(element)).GetAttribute("Value");
            else return String.Empty;*/
            return element.GetAttribute("value");

        }

        public static string GetTextFromDDL(IWebElement element /*string element, PropertyType elementtype*/)
        {
            /*if (elementtype == PropertyType.Id)
                return new SelectElement(SprocketPropertiesCollection.driver.FindElement(By.Id(element))).AllSelectedOptions.SingleOrDefault().Text;
            if (elementtype == PropertyType.Name)
                return new SelectElement(SprocketPropertiesCollection.driver.FindElement(By.Name(element))).AllSelectedOptions.SingleOrDefault().Text;
            if (elementtype == PropertyType.XPath)
                return new SelectElement(SprocketPropertiesCollection.driver.FindElement(By.XPath(element))).AllSelectedOptions.SingleOrDefault().Text;
            if (elementtype == PropertyType.CssSelector)
                return new SelectElement(SprocketPropertiesCollection.driver.FindElement(By.CssSelector(element))).AllSelectedOptions.SingleOrDefault().Text;
            if (elementtype == PropertyType.LinkText)
                return new SelectElement(SprocketPropertiesCollection.driver.FindElement(By.LinkText(element))).AllSelectedOptions.SingleOrDefault().Text;
            else return String.Empty;*/

            return new SelectElement(element).AllSelectedOptions.SingleOrDefault().Text;


        }
    }
}
