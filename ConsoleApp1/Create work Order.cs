﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

        }
        [SetUp]
        public void Initialize()
        {

            SprocketPropertiesCollection.driver = new ChromeDriver();
            SprocketPropertiesCollection.driver.Navigate().GoToUrl("http://43.powell.test/");
            Console.WriteLine("Opened URL");
        }

        [Test]
        public void ExecuteTest()
        {
            ExcelUtil.PopulateInCollection(@"C:\ConsoleApp1\Data.xlsx");


            LoginPage pageLogin = new LoginPage();

            EAPageObject pageEA = pageLogin.Login(ExcelUtil.ReadData(1, "UserName"), ExcelUtil.ReadData(1, "PassWord"));

            pageEA.CompleteWorkOrderDetails(ExcelUtil.ReadData(1, "Priority"), ExcelUtil.ReadData(1, "Craft"), ExcelUtil.ReadData(1, "Division"), ExcelUtil.ReadData(1, "WorkOrderTextFrame"));


        }
        [TearDown]
        public void CleanUp()
        {
            SprocketPropertiesCollection.driver.Close();
            SprocketPropertiesCollection.driver.Quit();
            Console.WriteLine("Close the browser");
        }


        }
    }
