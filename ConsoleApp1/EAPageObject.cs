﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;

namespace ConsoleApp1
{
    class EAPageObject
    {
        public EAPageObject()
        {
            PageFactory.InitElements(SprocketPropertiesCollection.driver, this);

        }

        [FindsBy(How = How.Id, Using = "ctl00_ContentPlaceHolder1_txtUserName")]
        public IWebElement UserName { get; set; }

        [FindsBy(How = How.Id, Using = "ctl00_ContentPlaceHolder1_txtPassword")]
        public IWebElement PassWord { get; set; }

        [FindsBy(How = How.Id, Using = "ctl00_ContentPlaceHolder1_btnLogin")]
        public IWebElement LoginButton { get; set; }

        [FindsBy(How = How.Id, Using = "ctl00_HeaderPage_lnkConsole_-7")]
        public IWebElement WorkManagementTab { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='btnNewLink']")]
        public IWebElement BtnNewWorkOrderLink { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#tabs_-7_menu > li:nth-child(2) > a")]
        public IWebElement ManageOverview { get; set; }

        [FindsBy(How = How.CssSelector, Using = "span.DropDownOption")]
        public IWebElement DDlRequestType { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#ddlRequestType_option_2 > span.DropDownOption")]
        public IWebElement DdlWorkOrderRequest { get; set; }

        [FindsBy(How = How.Id, Using = "StdSf34_PriorityID_txt")]
        public IWebElement PrioritySelection { get; set; }

        [FindsBy(How = How.Id, Using = "StdSf34_CraftID_txt")]
        public IWebElement CraftSelection { get; set; }

        [FindsBy(How = How.Id, Using = "StdSf34_DivisionID_txt")]
        public IWebElement DivisionSelection { get; set; }

        [FindsBy(How = How.Id, Using = "ctl00_MainPage_NewWorkOrder_RadEditor1_contentIframe")]
        public IWebElement WorkOrderTextFrame { get; set; }

        [FindsBy(How = How.Id, Using = "ctl00_MainPage_NewWorkOrder_btnSave")]
        public IWebElement SubmitWorkOrderButton { get; set; }

        [FindsBy(How = How.Id, Using = "ctl00_HeaderPage_lnkConsole_-6")]
        public IWebElement AdministrationButton { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='tabs_-6_menu']/li[6]/a")]
        public IWebElement WorkingManagement { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='tabs_-6_menu']/li[6]/ul/li[14]/a")]
        public IWebElement PeopleButton { get; set; }

        [FindsBy(How = How.Id, Using = "liSave")]
        public IWebElement SaveButton { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='btnNewLink']")]
        public IWebElement NewPersonButton { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='StdSf_62_UserName_txt']")]
        public IWebElement PersonUsername { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='StdSf_62_FirstName_txt']")]
        public IWebElement PersonFirstname { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='StdSf_62_LastName_txt']")]
        public IWebElement PersonLastname { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#tabs_-7_menu > li:nth-child(3) > a")]
        public IWebElement WorkOrdersButton { get; set; }

        [FindsBy(How = How.Id, Using = "ctl00_HeaderPage_lnkConsole_-3")]
        public IWebElement InventoryButton { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#tabs_-3_menu > li:nth-child(7) > a")]
        public IWebElement PurchaseRequestButton { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='btnNewLink']")]
        public IWebElement BtnNewPurchaseRequest { get; set; }

        [FindsBy(How = How.Id, Using = "StdSf17_VendorID_txt")]
        public IWebElement PurchaseVendor { get; set; }

        [FindsBy(How = How.Id, Using = "StdSf17_Comments_txt")]
        public IWebElement PurchaseComments { get; set; }

        [FindsBy(How = How.Id, Using = "StdSf17_ReferenceValue_txt")]
        public IWebElement POTracking { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='liSave']/a")]
        public IWebElement SavePurchase { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='liItems']/a")]
        public IWebElement ItemTab { get; set; }

        [FindsBy(How = How.Id, Using = "txtItemsFilter")]
        public IWebElement ItemText { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='ctl00_MainPage_NewPurchaseRequest_btnAddItem']")]
        public IWebElement AddItem { get; set; }

        [FindsBy(How = How.Id, Using = "btnSubmit")]
        public IWebElement SubmitPurchase { get; set; }

        [FindsBy(How = How.Id, Using = "btnApproveVote")]
        public IWebElement ApprovePurchase { get; set; }

        [FindsBy(How = How.Id, Using = "btnOrder")]
        public IWebElement OrderPurchase { get; set; }

        [FindsBy(How = How.Id, Using = "btnComplete")]
        public IWebElement ButtonCompletePurchase { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='StdSf17_VendorID_txt_magnifier']")]
        public IWebElement vendorMagnifier { get; set; }

        [FindsBy(How = How.XPath, Using = "#odaLookup_Modal_promptsdivContainer_0_input")]
        public IWebElement LookupText { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#lnkLookupGrid_Return_1")]
        public IWebElement LookupResult { get; set; }



        public void  CompleteWorkOrderDetails (string priority, string craft, string division, string WOTxtFrame)
        {
            WorkManagementTab.Clicks();
            ManageOverview.Clicks();
            BtnNewWorkOrderLink.Clicks();
            DDlRequestType.Clicks();
            DdlWorkOrderRequest.Clicks();
            PrioritySelection.Clicks();
            PrioritySelection.EnterText(priority);
            CraftSelection.Clicks();
            CraftSelection.EnterText(craft);
            DivisionSelection.Clicks();
            DivisionSelection.EnterText(division);
            WorkOrderTextFrame.EnterText(WOTxtFrame);
            SubmitWorkOrderButton.Clicks(); 


        }
        public void CreatePerson(string username, string firstname, string lastname)
        {
            AdministrationButton.Clicks();
            WorkingManagement.Clicks();
            PeopleButton.Clicks();
            NewPersonButton.Clicks();
            PersonUsername.EnterText(username);
            PersonFirstname.EnterText(firstname);
            PersonLastname.EnterText(lastname);
            SaveButton.Clicks();

        }

        public void CreatePurchaseRequest(string vendor, string trackingorder, string comments, string item)
        {

            
            InventoryButton.Clicks();
            PurchaseRequestButton.Clicks();
            BtnNewPurchaseRequest.Clicks();
            PurchaseComments.EnterText(comments);
            PurchaseVendor.EnterText(vendor);
            PurchaseVendor.SendKeys(Keys.Down+Keys.Enter);
            POTracking.EnterText(trackingorder);
            SavePurchase.Clicks();
            SprocketSetMethods.ElementIsVisible(ItemTab);
            ItemTab.Clicks();
            ItemText.EnterText(item);
            AddItem.Clicks();
            SubmitPurchase.Clicks();
            ApprovePurchase.Clicks();
            OrderPurchase.Clicks();
            ButtonCompletePurchase.Clicks();
           







        }
        



    }
}
