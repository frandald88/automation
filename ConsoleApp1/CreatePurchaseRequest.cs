﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class CreatePurchaseRequest
    {
        static void Main(string[] args)
        {

        }

        [SetUp]
        public void Initialize()
        {


            Console.WriteLine("Opened URL");
        }

        [Test]
        public void ExecuteTest()
        {

            ExcelUtil.PopulateInCollection(@"C:\ConsoleApp1\PurchaseRequest.xlsx");

            /*pageEA.CreatePerson(ExcelUtil.ReadData(1, "User"), ExcelUtil.ReadData(1, "FirstName"), ExcelUtil.ReadData(1, "LastName"));*/

            for (int row = 1; row < 10; row++)
            {
                SprocketPropertiesCollection.driver = new ChromeDriver();
                SprocketPropertiesCollection.driver.Navigate().GoToUrl("http://43.powell.test/");
                LoginPage pageLogin = new LoginPage();
                EAPageObject pageEA = pageLogin.Login(ExcelUtil.ReadData(row, "UserName"), ExcelUtil.ReadData(row, "PassWord"));
                pageEA.CreatePurchaseRequest(ExcelUtil.ReadData(row, "Vendor"), ExcelUtil.ReadData(row, "TrackingOrder"), ExcelUtil.ReadData(row, "Comments"), ExcelUtil.ReadData(row, "Item"));
                SprocketPropertiesCollection.driver.Close();
            }

        }

        [TearDown]
        public void CleanUp()
        {

            SprocketPropertiesCollection.driver.Quit();
            Console.WriteLine("Close the browser");
        }
    }
}