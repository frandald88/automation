﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public static class SprocketSetMethods
    {
        /// <summary>
        /// Extended method for entering text in the control
        /// </summary>
        ///  <param name="element"></param>
        ///  <param name="value"></param>
        public static void EnterText(this IWebElement element, string value  /*string element*//*PropertyType elementtype*/)
        {
            /*if (elementtype == PropertyType.Id)
                SprocketPropertiesCollection.driver.FindElement(By.Id(element)).SendKeys(value);
            if (elementtype == PropertyType.Name)
                SprocketPropertiesCollection.driver.FindElement(By.Name(element)).SendKeys(value);
            if (elementtype == PropertyType.XPath)
                SprocketPropertiesCollection.driver.FindElement(By.XPath(element)).SendKeys(value);
            if (elementtype == PropertyType.CssSelector)
                SprocketPropertiesCollection.driver.FindElement(By.CssSelector(element)).SendKeys(value);
            if (elementtype == PropertyType.LinkText)
                SprocketPropertiesCollection.driver.FindElement(By.LinkText(element)).SendKeys(value);*/

            element.SendKeys(value);


        }

        /// <summary>
        /// Click into a button, checkbox, option etc.
        /// </summary>
        ///  <param name="element"></param>
        public static void Clicks(this IWebElement element /*string element, PropertyType elementtype*/)
        {
            /*if (elementtype == PropertyType.Id)
                SprocketPropertiesCollection.driver.FindElement(By.Id(element)).Click();
            if (elementtype == PropertyType.Name)
                SprocketPropertiesCollection.driver.FindElement(By.Name(element)).Click();
            if (elementtype == PropertyType.XPath)
                SprocketPropertiesCollection.driver.FindElement(By.XPath(element)).Click();
            if (elementtype == PropertyType.CssSelector)
                SprocketPropertiesCollection.driver.FindElement(By.CssSelector(element)).Click();
            if(elementtype == PropertyType.LinkText)
                SprocketPropertiesCollection.driver.FindElement(By.LinkText(element)).Click();*/

            element.Click();
        }

        /// <summary>
        /// Select a dropdwon
        /// </summary>
        ///  <param name="element"></param>
        ///  <param name="value"></param>
        public static void SelectDropDown(this IWebElement element, string value /*string element*/  /*PropertyType elementtype*/)
        {

            /*if (elementtype == PropertyType.Id)
                new SelectElement(SprocketPropertiesCollection.driver.FindElement(By.Id(element))).SelectByText(value);
            if (elementtype == PropertyType.Name)
                new SelectElement(SprocketPropertiesCollection.driver.FindElement(By.Name(element))).SelectByText(value);
            if (elementtype == PropertyType.XPath)
                new SelectElement(SprocketPropertiesCollection.driver.FindElement(By.XPath(element))).SelectByText(value);
            if (elementtype == PropertyType.CssSelector)
                new SelectElement (SprocketPropertiesCollection.driver.FindElement(By.CssSelector(element))).SelectByText(value);
            if (elementtype == PropertyType.LinkText)
                new SelectElement (SprocketPropertiesCollection.driver.FindElement(By.LinkText(element))).SelectByText(value);*/

            new SelectElement(element).SelectByText(value);
        }

        public static void WaitForElementToBecomeVisibleWithinTimeout(IWebDriver driver,IWebElement element, int timeout)
        {
            new WebDriverWait(driver,
                TimeSpan.FromSeconds(timeout)).Until(ElementIsVisible(element));
        }

        public static Func<IWebDriver, bool> ElementIsVisible(IWebElement element)
        {
            return driver =>
            {
                try
                {
                    return element.Displayed;
                }
                catch (Exception)
                {
                    // If element is null, stale or if it cannot be located
                    return false;
                }

            };
            }

        public static void ScrollElementToBecomeVisible(IWebDriver driver, IWebElement element)
        {
            IJavaScriptExecutor jsExec = (IJavaScriptExecutor)driver;
            jsExec.ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }

        internal static void WaitForElementToBecomeVisibleWithinTimeout()
        {
            throw new NotImplementedException();
        }
    }
}


        
   