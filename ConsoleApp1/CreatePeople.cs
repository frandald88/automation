﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class CreatePeople
    {

        static void Main(string[] args)
        {

        }

        [SetUp]
        public void Initialize()
        {


            Console.WriteLine("Opened URL");
        }

        [Test]
        public void ExecuteTest()
        {

            ExcelUtil.PopulateInCollection(@"C:\ConsoleApp1\DataPerson.xlsx");

            /*pageEA.CreatePerson(ExcelUtil.ReadData(1, "User"), ExcelUtil.ReadData(1, "FirstName"), ExcelUtil.ReadData(1, "LastName"));*/

            for (int row = 1; row < 10; row++)
            {
                SprocketPropertiesCollection.driver = new ChromeDriver();
                SprocketPropertiesCollection.driver.Navigate().GoToUrl("http://43.powell.test/");
                LoginPage pageLogin = new LoginPage();
                EAPageObject pageEA = pageLogin.Login(ExcelUtil.ReadData(row, "UserName"), ExcelUtil.ReadData(row, "PassWord"));
                pageEA.CreatePerson(ExcelUtil.ReadData(row, "User"), ExcelUtil.ReadData(row, "FirstName"), ExcelUtil.ReadData(row, "LastName"));
                SprocketPropertiesCollection.driver.Close();
            }

        }

        [TearDown]
        public void CleanUp()
        {

            SprocketPropertiesCollection.driver.Quit();
            Console.WriteLine("Close the browser");
        }
    }
}